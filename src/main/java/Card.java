public interface Card {
     String replenishment();
     String debiting();
     String balanceRequest();

}
