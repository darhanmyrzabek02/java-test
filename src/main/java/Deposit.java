public interface Deposit {
    String replenishment();
    String closing();
    String balanceRequest();

}
