public class DebitCard extends BankProduct implements Card{
    @Override
    public String replenishment() {
        return "DebitCard replenishment";
    }

    @Override
    public String debiting() {
        return "DebitCard debiting";

    }

    @Override
    public String balanceRequest() {
        return "DebitCard balanceRequest";

    }
}
