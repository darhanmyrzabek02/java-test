public class CreditCard extends BankProduct implements Card {


    private double interestRate;

    @Override
    public String replenishment() {
        return "CreditCard replenishment";
    }

    @Override
    public String debiting() {
        return "CreditCard debiting";

    }

    @Override
    public String balanceRequest() {
       return "CreditCard balance request";

    }

    public String debtRequest()
    {
        return "CreditCard debt request";

    }


}
