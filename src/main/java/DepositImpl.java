public class DepositImpl extends BankProduct implements Deposit{
    @Override
    public String replenishment() {
        return "Deposity replenishment";
    }

    @Override
    public String closing() {
        return "Deposit Closing";
    }

    @Override
    public String balanceRequest() {
        return "Deposit Balance Request";
    }
}
