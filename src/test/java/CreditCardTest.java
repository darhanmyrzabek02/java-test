import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CreditCardTest {

    static CreditCard creditCard;

    @BeforeAll
    static void setup() {
        creditCard = new CreditCard();
    }
    @Test
    void checkCreditCardDebitingMethod()
    {
         creditCard = new CreditCard();
        Assertions.assertEquals("CreditCard debiting", creditCard.debiting());
    }

    @Test
    void checkCreditCardReplenishementMethod()
    {
         creditCard = new CreditCard();
        Assertions.assertEquals("CreditCard replenishment", creditCard.debiting());
    }

    @Test
    void checkCreditCardBalanceRequestMethod()
    {
         creditCard = new CreditCard();
        Assertions.assertEquals("CreditCard balance request", creditCard.debiting());
    }

}
